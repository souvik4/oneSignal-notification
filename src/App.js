import './App.css';
import oneSignalLogo from './images.jpeg';
// import OneSignal from 'react-onesignal';
// import { useEffect } from 'react';

function App() {

  // useEffect(() => {
  //    OneSignal.init({ appId: '4219b58d-7712-4b4a-8899-1d89ab7ce6c8' });
  // })
  

  const onHandleTag = (tag) => {
    console.log ("tagging");
    // OneSignal.sendTag('hii', tag).then(() => {
    //   console.log('Tagged');
    // })
  }

  return (
    <div className="App">
      <header className="App-header">
        <div>
        <img src={oneSignalLogo}  alt="logo" width="100%" height="100%"/>
        </div>
        <button className="btn btn-primary button" onClick={() => onHandleTag('react')}>ReactJs</button>
      </header>
    </div>
  );
}

export default App;
